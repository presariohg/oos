import com.sun.glass.ui.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AnmeldungsController {

    @FXML public Button btStart;
    @FXML public TextField tfUserId;
    @FXML public PasswordField tfPassword;
    @FXML public PasswordField tfPasswordRepeat;
    @FXML public Text textAlert;
    private boolean isShowingAlert = false;

    Main mainApplication;

    public void setMainApplication(Main mainApplication) {
        this.mainApplication = mainApplication;
    }

    /**
     * Check if the password and password repeat fields have matching values. If they do, create a new {@link Benutzer},
     * print out and exit. If not, show an alert.
     * @param actionEvent Action event of this click
     */
    @FXML
    public void startClicked(ActionEvent actionEvent) throws Exception {
        String password = tfPassword.getText();
        String passwordRepeat = tfPasswordRepeat.getText();

        if (password.equals(passwordRepeat)) {
            String userId = tfUserId.getText();
            Benutzer user = new Benutzer(userId, password.toCharArray());

            this.mainApplication.neuerBenutzer(user);

        } else {
            showAlert("Password != Password repeat");
        }
    }


    /**
     * Show an alert to the scene
     * @param text The alert to be shown
     */
    public void showAlert(String text) {
        isShowingAlert = true;
        textAlert.setVisible(true);
        textAlert.setText(text);
    }


    /**
     * Hide the password repeat not equal alert when clicked on one of the two password fields
     */
    @FXML
    public void hideAlert() {
        if (isShowingAlert) {
            // select all whichever field is being focused on
            tfPassword.selectAll();
            tfPasswordRepeat.selectAll();

            // set alert to invisible
            isShowingAlert = false;
            textAlert.setVisible(false);
        }
    }


}
