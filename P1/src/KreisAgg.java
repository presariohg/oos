public class KreisAgg extends Shape <KreisAgg>{
    Point centre;

    double radius;

    KreisAgg() {}

    KreisAgg(Point centre, double radius) {
        this.centre = centre;
        this.radius = radius;
    }

    KreisAgg(KreisAgg reference) {
        this.centre = reference.centre;
        this.radius = reference.radius;
    }


    public Point getCentre() {
        return centre;
    }

    public void setCentre(Point centre) {
        this.centre = centre;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public double getArea() {
        if (area == -1) {// never calculated
            area = radius * radius * Math.PI;
        }

        return area;
    }

    @Override
    public boolean equals(KreisAgg another) {
        return ((this.radius == another.radius) &&
                (this.centre.equals(another.centre)));
    }

    @Override
    protected KreisAgg clone() {
        return new KreisAgg(this);
    }

    @Override
    public String toString() {
        return "Circle centre = " + centre + ", radius = " + radius;
    }


}
