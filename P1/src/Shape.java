public abstract class Shape <S extends Shape> {
    protected double area = -1;

    abstract double getArea();

    abstract public boolean equals(S another);

    abstract protected S clone();

    abstract public String toString();
}
