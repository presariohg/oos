import com.sun.glass.ui.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

import static javafx.application.Platform.exit;

public class LoginController {

    private boolean neuAnmeldung = false;

    @FXML public Button btStart;
    @FXML public CheckBox cbRegister;
    @FXML public TextField tfUserId;
    @FXML public PasswordField tfPassword;
    @FXML public Text textAlert;

    private boolean isShowingAlert = false;

    Main mainApplication;


    public void setMainApplication(Main mainApplication) {
        this.mainApplication = mainApplication;
    }


    /**
     * Create a {@link Benutzer} out of the provided info on the fields. Print out this {@link Benutzer} along with
     * the result of the checkbox, then exit
     * @param actionEvent Action event of this click
     */
    public void startClicked(ActionEvent actionEvent) throws IOException {
        neuAnmeldung = cbRegister.isSelected();

        if (neuAnmeldung) {
            this.mainApplication.neuAnmeldung();

            // close this login window
            Stage loginStage = (Stage) btStart.getScene().getWindow();
            loginStage.close();
        } else {
            String userId = tfUserId.getText();
            String password = tfPassword.getText();

            Benutzer user = new Benutzer(userId, password.toCharArray());

            this.mainApplication.benutzerLogin(user);
        }
    }


    /**
     * Show an alert to the scene
     * @param text The alert to be shown
     */
    public void showAlert(String text) {
        isShowingAlert = true;
        textAlert.setVisible(true);
        textAlert.setText(text);
    }


    /**
     * Hide the password repeat not equal alert when clicked on one of the two password fields
     */
    @FXML
    public void hideAlert() {
        if (isShowingAlert) {
            // select all whichever field is being focused on
            tfPassword.selectAll();
            tfUserId.selectAll();

            // set alert to invisible
            isShowingAlert = false;
            textAlert.setVisible(false);
        }
    }


    /**
     * Show the status of the checkbox when clicked on.
     */
    public void cbClicked() {
        neuAnmeldung = cbRegister.isSelected();
        System.out.println("neu anmeldung = " + neuAnmeldung);

    }
}
