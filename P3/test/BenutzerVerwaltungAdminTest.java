import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test methods for Praktikum 3")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BenutzerVerwaltungAdminTest {
    static BenutzerVerwaltungAdmin admin;
    static LinkedList<Benutzer> userList = new LinkedList<>();
    @BeforeAll
    static void setUp() {
        admin = new BenutzerVerwaltungAdmin();
        userList.addLast(new Benutzer("foo", "password".toCharArray()));
        userList.addLast(new Benutzer("bar", "p4ssword".toCharArray()));
        userList.addLast(new Benutzer("anyideas", "p455word".toCharArray()));
        userList.addLast(new Benutzer("someid", "p455vvord".toCharArray()));
        userList.addLast(new Benutzer("outofnames", "p455vv0rd".toCharArray()));
    }



    static Stream<Benutzer> benutzerArgProvider() {
        return userList.stream();
    }



    @Test @Order(1)
    @DisplayName("Unit Tests for user list manipulations, after reading from persistent file:")
    void userListManipulation() {}


    @ParameterizedTest @Order(2)
    @MethodSource("benutzerArgProvider")
    @DisplayName("Normal Queries")
    void normalQueries(Benutzer user) {
        assertDoesNotThrow(
            () -> assertTrue(admin.benutzerVorhanden(user)));

    }


    @Test @Order(3)
    @DisplayName("Invalid User Queries")
    void invalidUserQueries() {
        assertThrows(Benutzer.InvalidUserException.class,
            () -> admin.benutzerVorhanden(null));
    }


    @Test @Order(4)
    @DisplayName("Delete users from list")
    void deleteUser() {
        assertDoesNotThrow(
            () -> admin.benutzerLoeschen(userList.get(0)));
    }


    @Test @Order(5)
    @DisplayName("Query a deleted (not existent) user")
    void queryNonExistUser() {
        assertDoesNotThrow(
            () -> assertFalse(admin.benutzerVorhanden(userList.get(0))));
    }


    @Test @Order(6)
    @DisplayName("Delete a non-existent user")
    void deleteNullUser() {
        assertThrows(Benutzer.UserDoesNotExistException.class,
            () -> admin.benutzerLoeschen(userList.get(0)));
    }


    @Test @Order(7)
    @DisplayName("Drop all users")
    void dropUsers() {
        assertDoesNotThrow(
            () -> admin.dbInit());
//        admin.dropTable();
        assertDoesNotThrow(
            () -> assertEquals(admin.getUserList(), new LinkedList<>()));
    }


    @Test @Order(8)
    @DisplayName("Unit Tests for user entries:")
    void benutzerEintragenTest() {}


    @ParameterizedTest @Order(9)
    @MethodSource("benutzerArgProvider")
    @DisplayName("Normal Entries")
    void normalEntries(Benutzer user) {
        assertDoesNotThrow(
            () -> admin.benutzerEintragen(user));
    }


    @Test @Order(10)
    @DisplayName("Duplicated Entries")
    void duplicatedEntries() {
        assertThrows(Benutzer.UserAlreadyExistsException.class,
            () -> admin.benutzerEintragen(userList.get(0)));
    }


    @Test @Order(11)
    @DisplayName("Invalid Entries")
    void invalidEntries() {
        assertThrows(Benutzer.InvalidUserException.class,
            () -> admin.benutzerEintragen(null));
    }


    @Test @Order(12)
    @DisplayName("Short Password Entries")
    void shortPassword() {
        assertThrows(Benutzer.PasswordTooShortException.class,
            () -> admin.benutzerEintragen(new Benutzer("short_password", "1".toCharArray())));
    }
}
