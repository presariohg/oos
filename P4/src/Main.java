import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Stage loginStage = new Stage();
        Stage anmeldungStage = new Stage();

        Parent anwendungScence = FXMLLoader.load(getClass().getResource("anwendung.fxml"));
        Parent loginScene = FXMLLoader.load(getClass().getResource("login.fxml"));
        Parent anmeldungScene = FXMLLoader.load(getClass().getResource("anmeldung.fxml"));

        // set each stage to its matching scene
        loginStage.setTitle("Benutzerverwaltung");
        loginStage.setScene(new Scene(loginScene));

        anmeldungStage.setTitle("Benutzerverwaltung");
        anmeldungStage.setScene(new Scene(anmeldungScene));

        primaryStage.setTitle("Benutzerverwaltung");
        primaryStage.setScene(new Scene(anwendungScence));


        // show all 3 windows at the same time
        primaryStage.show();
        loginStage.show();
        anmeldungStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
