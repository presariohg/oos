public class P1 {
    public static void main(String[] args) {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(1, 1);

        KreisVererb c1 = new KreisVererb(p1, 3);
        KreisVererb c2 = new KreisVererb(p2, 2);
        KreisVererb c3 = new KreisVererb(p1, 3);

        System.out.println("Circle 1 (c1) = " + c1);
        System.out.println("Circle 2 (c2) = " + c2);
        System.out.println("Circle 3 (c3) = " + c3);

        System.out.println();

        System.out.println("c1 equals c2 = " + c1.equals(c2));
        System.out.println("c3 equals c2 = " + c3.equals(c2));
        System.out.println("c3 equals c1 = " + c1.equals(c3));

        c3.setRadius(c1.radius);

        System.out.println();

        System.out.println("Changed c3 into: " + c3);
        System.out.println("c3 equals c1 = " + c1.equals(c3));


        Shape[] arr = new Shape[4];

        arr[0] = new Rechteck(new Point(1, 0), new Point(0, 1));
        arr[1] = new KreisAgg(new Point(0, 0), 2);
        arr[2] = new Rechteck(p1, p2);
        arr[3] = new KreisAgg(c1.getCentre(), c1.getRadius());

        for (Shape shape : arr) {
            System.out.println(shape);
            System.out.println("Area = " + shape.getArea());
            System.out.println();
        }
    }
}
