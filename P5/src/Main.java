import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main extends Application {
    private final BenutzerVerwaltungAdmin admin = new BenutzerVerwaltungAdmin();
    private final FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("login.fxml"));
    private final FXMLLoader anmeldungLoader = new FXMLLoader(getClass().getResource("anmeldung.fxml"));
    private final FXMLLoader anwendungLoader = new FXMLLoader(getClass().getResource("anwendung.fxml"));


    /**
     * //TODO: write comment for this method
     */
    private boolean queryInitDb() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        boolean isValid = false;
        boolean result = false;
        do {
            System.out.println("Init db? (1 = y, 0 = n):");
            try {
                int dbInit = Integer.parseInt(br.readLine());
                if (!((dbInit == 1)
                    || (dbInit == 0))) {
                    continue;
                }

                result = dbInit == 1;
                isValid = true;
            } catch (Exception e) {
                System.out.println("Error parsing command");
            }

        } while (!isValid);

        return result;
    }


    /**
     * Show the register screen
     */
    public void neuAnmeldung() throws IOException {
        AnmeldungsController anmeldungsController;
        // load the scene the second time will result in an exception
        if (anmeldungLoader.getRoot() == null) {
            Scene anmeldungScene = new Scene(anmeldungLoader.load());
            anmeldungsController = anmeldungLoader.getController();
        } else {
            anmeldungsController = anmeldungLoader.getController();
            Scene anmeldungScene = anmeldungsController.btStart.getScene();
        }

        // set anmeldung controller's main application attr to this application
        anmeldungsController.setMainApplication(this);

        // auto fill the user id & password field in register page
        LoginController loginController = loginLoader.getController();
        anmeldungsController.tfUserId.setText(loginController.tfUserId.getText());
        anmeldungsController.tfPassword.setText(loginController.tfPassword.getText());

        Stage anmeldungStage = new Stage();

        anmeldungStage.setTitle("Registrieren");
        anmeldungStage.setScene(anmeldungsController.textAlert.getScene());
        anmeldungStage.show();
    }


    /**
     * Validate a new user creation request, show alert if invalid, otherwise show the login screen after that
     * @param benutzer The new user to be added in
     */
    public void neuerBenutzer(Benutzer benutzer) throws IOException {
        AnmeldungsController anmeldungController = anmeldungLoader.getController();
        boolean isValid = false;
        try {
            admin.benutzerEintragen(benutzer);
            isValid = true;
        } catch (Benutzer.UserAlreadyExistsException e) {
            anmeldungController.showAlert("Username already exists!");
        } catch (Benutzer.InvalidUserException e) {
            anmeldungController.showAlert("This user is invalid!");
        } catch (Benutzer.PasswordTooShortException e) {
            anmeldungController.showAlert("Password must be longer than 3 characters!");
        } catch (ClassNotFoundException e) {
            anmeldungController.showAlert("Fatal error!");
        }

        if (isValid) {
            // autofill the user and password field
            LoginController loginController = loginLoader.getController();
            loginController.tfUserId.setText(benutzer.userId);
            loginController.tfPassword.setText(String.valueOf(benutzer.passWort));
            loginController.cbRegister.setSelected(false);
            loginController.hideAlert();

            // close the register page
            Stage anmeldungStage = (Stage) anmeldungController.textAlert.getScene().getWindow();
            anmeldungStage.close();

            // show the login page
            Stage loginStage = new Stage();
            loginStage.setTitle("Login");
            loginStage.setScene(loginController.btStart.getScene());
            loginStage.show();
        }
    }


    /**
     * Check if this user exists in db, if not show an alert, otherwise show the {@link AnwendungController}'s view
     * @param benutzer The user to login with
     */
    public void benutzerLogin(Benutzer benutzer) {
        LoginController loginController = loginLoader.getController();

        try {
            if (admin.benutzerVorhanden(benutzer)) {
                //close login window
                Stage loginStage = (Stage) loginController.tfUserId.getScene().getWindow();
                loginStage.close();

                // show app window
                Stage anwendungStage = new Stage();
                anwendungStage.setScene(new Scene(anwendungLoader.load()));
                anwendungStage.show();
            } else {
                loginController.showAlert("UserID or password is false");
            }
        } catch (ClassNotFoundException | IOException e) {
            loginController.showAlert("Fatal Error!");
        } catch (Benutzer.InvalidUserException e) {
            loginController.showAlert("Invalid user!");
        }
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        boolean dbInit = queryInitDb();

        if (dbInit)
            admin.dbInit();

        Parent loginScene = loginLoader.load();
        Stage loginStage = new Stage();

        LoginController loginController = loginLoader.getController();
        loginController.setMainApplication(this);

        // show the login stage
        loginStage.setTitle("Login");
        loginStage.setScene(new Scene(loginScene));

        loginStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
