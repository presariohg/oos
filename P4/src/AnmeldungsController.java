import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

public class AnmeldungsController {

    @FXML public Button btStart;
    @FXML public TextField tfUserId;
    @FXML public PasswordField tfPassword;
    @FXML public PasswordField tfPasswordRepeat;
    @FXML public Text textAlertPasswordRepeat;
    private boolean isShowingAlert = false;

    /**
     * Check if the password and password repeat fields have matching values. If they do, create a new {@link Benutzer},
     * print out and exit. If not, show an alert.
     * @param actionEvent Action event of this click
     */
    @FXML
    public void startClicked(ActionEvent actionEvent) {
        String password = tfPassword.getText();
        String passwordRepeat = tfPasswordRepeat.getText();

        if (password.equals(passwordRepeat)) {
            String userId = tfUserId.getText();
            Benutzer user = new Benutzer(userId, password.toCharArray());

            System.out.println("ID: " + user.userId + ", Password: " + String.valueOf(user.passWort));
            Stage stage = (Stage) btStart.getScene().getWindow();
            stage.close();
        } else {
            isShowingAlert = true;
            textAlertPasswordRepeat.setVisible(true);
        }

    }

    /**
     * Hide the password repeat not equal alert when clicked on one of the two password fields
     */
    @FXML
    public void hideAlert() {
        if (isShowingAlert) {
            // select all whichever field is being focused on
            tfPassword.selectAll();
            tfPasswordRepeat.selectAll();

            // set alert to invisible
            isShowingAlert = false;
            textAlertPasswordRepeat.setVisible(false);
        }
    }


}
