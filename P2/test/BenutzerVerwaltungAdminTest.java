import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test methods for Praktikum 2")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BenutzerVerwaltungAdminTest {
    static final BenutzerVerwaltungAdmin admin = new BenutzerVerwaltungAdmin();
    static final LinkedList<Benutzer> userList = new LinkedList<>();
    @BeforeAll
    static void setUp() {
        userList.addLast(new Benutzer("foo", "password".toCharArray()));
        userList.addLast(new Benutzer("bar", "p4ssword".toCharArray()));
        userList.addLast(new Benutzer("anyideas", "p455word".toCharArray()));
        userList.addLast(new Benutzer("someid", "p455vvord".toCharArray()));
        userList.addLast(new Benutzer("outofnames", "p455vv0rd".toCharArray()));
    }


    static Stream<Benutzer> benutzerArgProvider() {
        return userList.stream();
    }


    @Test @Order(0)
    @DisplayName("Unit Tests for user entries:")
    void benutzerEintragenTest() {}


    @ParameterizedTest @Order(1)
    @MethodSource("benutzerArgProvider")
    @DisplayName("Normal Entries")
    void normalEntries(Benutzer user) {
        assertDoesNotThrow(() -> admin.benutzerEintragen(user));
    }


    @Test @Order(2)
    @DisplayName("Duplicated Entries")
    void duplicatedEntries() {
        assertThrows(Benutzer.UserAlreadyExistsException.class,
            () -> admin.benutzerEintragen(userList.get(0)));
    }


    @Test @Order(3)
    @DisplayName("Invalid Entries")
    void invalidEntries() {
        assertThrows(Benutzer.InvalidUserException.class,
            () -> admin.benutzerEintragen(null));
    }


    @Test @Order(4)
    @DisplayName("Short Password Entries")
    void shortPassword() {
        assertThrows(Benutzer.PasswordTooShortException.class,
            () -> admin.benutzerEintragen(new Benutzer("short_password", "1".toCharArray())));
    }


    @Test @Order(5)
    @DisplayName("Unit Tests for user list manipulations:")
    void userListManipulation() {}


    @ParameterizedTest @Order(6)
    @MethodSource("benutzerArgProvider")
    @DisplayName("Normal Queries")
    void normalQueries(Benutzer user) {
        try {
            assertTrue(admin.benutzerVorhanden(user));
        } catch (Benutzer.InvalidUserException e) {
            fail();
        }
    }


    @Test @Order(7)
    @DisplayName("Invalid User Queries")
    void invalidUserQueries() {
        assertThrows(Benutzer.InvalidUserException.class,
            () -> admin.benutzerVorhanden(null));
    }

    @Test @Order(8)
    @DisplayName("Delete users from list")
    void deleteUser() {
        assertDoesNotThrow(() -> admin.benutzerLoeschen(userList.get(0)));
    }

    @Test @Order(9)
    @DisplayName("Query a deleted (not existent) user")
    void queryNonExistUser() {
        try {
            assertFalse(admin.benutzerVorhanden(userList.get(0)));
        } catch (Benutzer.InvalidUserException e) {
            fail();
        }
    }

    @Test @Order(10)
    @DisplayName("Delete a non-existent user")
    void deleteNullUser() {
        assertThrows(Benutzer.UserDoesNotExistException.class,
            () -> admin.benutzerLoeschen(userList.get(0)));
    }

    @Test @Order(11)
    @DisplayName("Drop all users")
    void dropUsers() {
        admin.dropTable();
        assertEquals(admin.getUserList(), new LinkedList<>());
    }
}