import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Arrays;

import static javafx.application.Platform.exit;

public class LoginController {

    private boolean neuAnmeldung = false;

    @FXML public Button btStart;
    @FXML public CheckBox cbRegister;
    @FXML public TextField tfUserId;
    @FXML public PasswordField tfPassword;

    /**
     * Create a {@link Benutzer} out of the provided info on the fields. Print out this {@link Benutzer} along with
     * the result of the checkbox, then exit
     * @param actionEvent Action event of this click
     */
    public void startClicked(ActionEvent actionEvent) {
        neuAnmeldung = cbRegister.isSelected();


        String userId = tfUserId.getText();
        String password = tfPassword.getText();

        Benutzer user = new Benutzer(userId, password.toCharArray());


        System.out.println("ID: " + user.userId + ", Password: " + String.valueOf(user.passWort));
        System.out.println(neuAnmeldung);
        Stage stage = (Stage) btStart.getScene().getWindow();
        stage.close();

    }

    /**
     * Show the status of the checkbox when clicked on.
     */
    public void cbClicked() {
        neuAnmeldung = cbRegister.isSelected();
        System.out.println("neu anmeldung = " + neuAnmeldung);
    }
}
