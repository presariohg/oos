import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

public class AnwendungController {
    public Button buttonAbbrechen;
    @FXML Text textMain;

    public void cancel(ActionEvent actionEvent) {
        System.out.println("Abbrechen clicked");
//        exit();
        Stage stage = (Stage) buttonAbbrechen.getScene().getWindow();
        stage.close();
    }

}
